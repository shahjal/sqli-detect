--[[
 
     Uses MySQL-Proxy to log queries and detect sql injection attempts
     using a simple key matching algorithm. only written to protect
     our team's webapp in CTFs A/D round ;)
     
     Written by Shahriyar Jalayeri
     twitter.com/ponez
 
--]]

local commands = {"select", "update", "union", "insert", "where"}
local log_query_file = 'mysql_query.log'
local log_inject_file = 'mysql_inject_query.log'
local log_fh = io.open(log_query_file, "a+")
local inj_fh = io.open(log_inject_file, "a+")
printf = function(s,...) return io.write(s:format(...)) end -- function

function match_and_fix( query, command, key )
    local a, b = 0, 0
    local query_key = ""
    local match_found = false

    a,b = string.find(string.upper(query), string.upper(command), 0, true)
    while true do
        if  a == nil then
            if match_found == true then
                printf("[OLD QUERY] %s \n", query)
                query = string.gsub(string.upper(query), string.upper(command) .. string.upper(key), string.upper(command))
                changed = true
                printf("[NEW QUERY] %s \n", query) 
            end
            return query, true
        end

        query_key = string.sub(string.upper(query), b+1 , b+3)
        if query_key == key then
            printf("[KEY MATCH] \"%s\" at %d with key \"%s\"\n", command, a, query_key)
            match_found = true
        else
            printf("[KEY MISMATCH] \"%s\" at %d with key \"%s\"\n", command, a, query_key)
            return "SQLI_ERRRO", true
        end
        a,b = string.find(string.upper(query), string.upper(command), b + string.len(key), true)
    end
end

function read_query( packet )
    
    if string.byte(packet) == proxy.COM_QUERY then
        local query = string.sub(packet, 2)
        local changed = false
        
        for k,v in pairs(commands) do
            query, changed = match_and_fix(query, v, "KEY_IS_SHUTUP_YOU_STUPID_DWANG")
        end
        
        if query == "SQLI_ERRRO" then 
            log_query( packet, true ) 
        else 
            log_query( packet, false ) 
        end

        if changed == true then
            printf("[SENDING QUERY] %s\n", query)
            printf("-------------------------------------------------\n") 
            proxy.queries:append(1, string.char(proxy.COM_QUERY) .. query )
            return proxy.PROXY_SEND_QUERY
        end
    end
end

function log_query( packet, inject )
    local fh = log_fh
    if inject == true then fh = inj_fh end
    if string.byte(packet) == proxy.COM_QUERY then
        local query = string.sub(packet, 2)
        fh:write( string.format("%s %6d -- %s \n", 
            os.date('%Y-%m-%d %H:%M:%S'), 
            proxy.connection.server["thread_id"], 
            query)) 
        fh:flush()
    end
end
